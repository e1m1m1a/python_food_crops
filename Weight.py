from Unit import Unit


class Weight(Unit): # hérite de Unit

    def __init__(self, id: int, multiplier: float, name: str = "Weight"):
        super().__init__(id, name)
        self.multiplier = multiplier

    def describe(self): # la méthode est adpatée à l'appel de describe appliqué à une mesure
            return self.name
