from enum import Enum


class CommodityGroup(Enum): # hérite de Enum
    CORN = 12
    BARLEY = 9
    OATS = 17
    SORGHUM = 20
    BYPRODUCTS_FEEDS = 10
    COARSE_GRAINS = 11
    HAY = 16
    FEED_GRAINS = 14
    ANIMAL_PROTEIN_FEEDS = 8
    GRAIN_PROTEIN_FEEDS = 15
    PROCESSED_FEEDS = 19
    ENERGY_FEEDS = 13
    OTHER = 18  # oilseed meal feeds? (incohérence entre les données fournies)