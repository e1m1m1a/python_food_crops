import pandas

from FoodCropFactory import FoodCropFactory
from Unit import Unit
from CommodityGroup import CommodityGroup
from IndicatorGroup import IndicatorGroup


class FoodCropsDataset:

# Constructeur
    def __init__(self, commodityGroupMeasurement: dict, indicatorGroupMeasurement: dict, locationMeasurement: dict,
                 unitMeasurement: dict):
        self.commodityGroupMeasurement = commodityGroupMeasurement
        self.indicatorGroupMeasurement = indicatorGroupMeasurement
        self.locationMeasurement = locationMeasurement
        self.unitMeasurement = unitMeasurement

# Méthode de chargement du fichier
    def load(self, datasetPath: str):

        fcf = FoodCropFactory(dict(), dict(), dict())  # crée un objet FoodCropFactory qui permettra de créer des
        # instances sans faire de doublons
        dataframe = pandas.read_csv(datasetPath)  # récupération du fichier csv contenant les mesures
        dataframe = dataframe.dropna()  # suppression des mesures contenant des NaN, afin de ne pas générer d'erreurs
        # à la conversion en int ou str

        for index, row in dataframe.iterrows():  # parcours de l'ensemble des lignes du fichier
        # récupération de geographicalLocation
            geographicalLocation = str(row['SC_GeographyIndented_Desc'])
        # récupération de commodityGroup
            commodityGroup = CommodityGroup(int(row['SC_GroupCommod_ID'])) # (enumClass(int) retourne un élément de
            # type enum dont l'int correspond à la valeur associée)
        # récupération de indicatorGroup
            indicatorGroup = IndicatorGroup(int(row['SC_Group_ID']))
        # création de unit
            idUnit = int(row['SC_Unit_ID'])
            # récupération des indices pour "per", "ton" et "hectare" car ton et hectare sont les seules unités
            # pouvant être avant ou après le "par". On travail sur la comparaison des indices et non pas avec les
            # chaînes de caractères, "per ton" par exemple, car "per" et "ton" peuvent être séparés d'un mot
            # intermédiaire.
            indicePer = int(row['SC_Unit_Desc'].find("per"))
            indiceTon = int(row['SC_Unit_Desc'].find("ton"))
            indiceHectare = row['SC_Unit_Desc'].find("hectare")
            # tests vérifiant si la chaîne de caractères contient un mot clef indiquant un type d'unité.
            # On ne prend pas en compte la première lettre qui peut être entrée en majuscule ou miniscule (le reste du
            # mot suffit pour le test).
            if "uschels" in row['SC_Unit_Desc']:
                unit = fcf.createVolume(idUnit)
            elif "cres" in row['SC_Unit_Desc']:
                unit = fcf.createSurface(idUnit)
            elif "ollars" in row['SC_Unit_Desc']:
                unit = fcf.createPrice(idUnit)
            elif "atio" in row['SC_Unit_Desc']:
                unit = fcf.createRatio(idUnit)
            elif "ents" in row['SC_Unit_Desc']:
                unit = fcf.createPrice(idUnit)
            elif "iters" in row['SC_Unit_Desc']:
                unit = fcf.createVolume(idUnit)
            elif "allons" in row['SC_Unit_Desc']:
                unit = fcf.createVolume(idUnit)
            elif indicePer == -1 and indiceTon != -1:  # vérifie si "ton" est présent mais pas "per". Dans ce cas
                # l'unité est bien weight
                if "1000" in row['SC_Unit_Desc']:
                    unit = fcf.createWeight(idUnit,1000)  # "1000 metric tons" est la seule unité de poids que l'on a
                    # trouvé avec un facteur multiplicateur, on effectue donc le test que pour 1000
                else:
                    unit = fcf.createWeight(idUnit, 1)
            elif indicePer > indiceTon >= 0:  # on effectue ici le test sur l'ordre d'apparition du mot "ton" (il ne
                # définit le type de l'unité que si il s'agit de "ton" "per" et non de "per" "ton")
                if "1000" in row['SC_Unit_Desc']:
                    unit = fcf.createWeight(idUnit, 1000)
                else:
                    unit = fcf.createWeight(idUnit, 1)
            elif "Ton" in row[
                'SC_Unit_Desc']:  # après vérification du fichier csv, nous avons trouvé que si "ton" débutait avec
                # une majuscule, alors l'unité était "ton"
                if "1000" in row['SC_Unit_Desc']:
                    unit = fcf.createWeight(idUnit, 1000)
                else:
                    unit = fcf.createWeight(idUnit, 1)
            elif indicePer == -1 and indiceHectare != -1:  # idem que pour "ton"
                unit = fcf.createSurface(idUnit)
            elif indicePer > indiceHectare >= 0:
                unit = fcf.createSurface(idUnit)
            elif "Hectare" in row['SC_Unit_Desc']:
                unit = fcf.createSurface(idUnit)
            else:
                unit = fcf.createCount(idUnit, str(row['SC_Unit_Desc']))  # doutes sur le paramètre "what" (
                # 'SC_Unit_Desc' ou 'SC_Commodity_Desc' ?)
        # création de commodity à partir de commodityGroup
            commodity = fcf.createCommodity(commodityGroup, int(row['SC_Commodity_ID']),
                                            row['SC_Commodity_Desc'])
        # création de indicator à partir de unit, indicatorGroup et geographicalLocation
            indicator = fcf.createIndicator(int(row['SC_Attribute_ID']),
                                            int(row['SC_Frequency_ID']),
                                            str(row['SC_Frequency_Desc']),
                                            geographicalLocation, indicatorGroup, unit)
        # création de la mesure à partir de commodity et indicator
            measurement = fcf.createMeasurement(index, int(row['Year_ID']), float(row['Amount']),
                                                int(row['Timeperiod_ID']), row['Timeperiod_Desc'],
                                                commodity, indicator)
        # ajout de la mesure à la bonne clef dans les 4 dictionnaires
            # dictionnaire commodityGroupMeasurement
            if commodityGroup in self.commodityGroupMeasurement:  # on vérifie si la clef commodityGroup existe déjà
                # dans le dictionnaire
                self.commodityGroupMeasurement[commodityGroup] += [
                    measurement]  # dans ce cas, on ajoute la mesure à la liste de cette clef
            else:  # dans le cas où la clef n'existe pas encore
                self.commodityGroupMeasurement[commodityGroup] = [
                    measurement]  # on initialise sa valeur à un tableau contenant la mesure actuelle
            # dictonnaire indicatorGroupMeasurement
            if indicatorGroup in self.indicatorGroupMeasurement:  # idem que pour commodityGroup
                self.indicatorGroupMeasurement[indicatorGroup] += [measurement]
            else:
                self.indicatorGroupMeasurement[indicatorGroup] = [measurement]
            # dictionnaire locationMeasurement
            if geographicalLocation in self.locationMeasurement:  # idem que pour commodityGroup
                self.locationMeasurement[geographicalLocation] += [measurement]
            else:
                self.locationMeasurement[geographicalLocation] = [measurement]
            # dictionnaire unitMeasurement
            if unit in self.unitMeasurement:  # idem que pour commodityGroup
                self.unitMeasurement[unit] += [measurement]
            else:
                self.unitMeasurement[unit] = [measurement]

# Méthode de recherche de mesures selon commodityGroup, indicatorGroup, unit et geogLocation
    def findMeasurements(self, commodityGroup: CommodityGroup = None,
                         indicatorGroup: IndicatorGroup = None, geogLocation: str = None,
                         unit: Unit = None):

        measurementsList = []  # on initialise une liste de mesures qui va contenir toutes celles que l'on souhaite
        # renvoyer

    # on effectue des tests sur les paramètres unit et geogLocation afin d'être sûrs qu'ils puissent être comparés
    # avec ceux des mesures de la méthode load
        if unit is not None:  # le test n'est nécessaire que si unit n'est pas nul et devra être comparé
            for u in self.unitMeasurement.keys():  # évite le problème de 2 objets unit avec mêmes attributs mais
                # différents car crées dans différents espaces mémoire
                if u.id == unit.id:  # on vérifie si l'id de l'unité en paramètre correspond à l'id d'une des clefs
                    # de unitMeasurement (id est une primary key en SQL)
                    unit = u  # dans le cas où on trouve une correspondance, on change unit en l'unité étant clef
                    # dans unitMeasurement pour que les tests de comparaison par la suite soient fonctionnels
        if geogLocation is not None:  # idem que le test sur unit, permet d'être sûrs d'avoir la bonne clef pour
            # appeler locationMeasurement
            for gl in self.locationMeasurement.keys():
                if geogLocation in gl:
                    geogLocation = gl

    # cas où l'ensemble des paramètres est nul
        if commodityGroup is None and indicatorGroup is None and geogLocation is None and unit is None:
            for key, value in self.indicatorGroupMeasurement.items():  # on choisit aléatoirement de parcourir les
                # mesures de indicatorGroupMeasurement
                measurementsList += value  # on ajoute l'ensemble des mesures à la liste de retour (certaines mesures
                # manqueront, il s'agit de celles contenant des NaN)

    # cas où commodityGroup n'est pas nul
        elif commodityGroup is not None:  # ne permet de récupérer des mesures que dans le cas où commodityGroup
            # n'est pas nul
            for measurement in self.commodityGroupMeasurement[commodityGroup]:  # on parcourt l'ensemble des mesures
                # qui sont rangées sous la clef commodityGroup (on aurait une erreur si on ne testait pas la non
                # nulité avant)
                if indicatorGroup is None or (
                        indicatorGroup is not None and measurement in self.indicatorGroupMeasurement[
                    indicatorGroup]):
                    # Si indicatorGroup est nul, le test est validé et on conserve toutes les mesures (indicatorGroup
                    # n'est pas un paramètre restrictif dans la recherche). En revanche, s'il n'est pas nul,
                    # seules les mesures présentes dans indicatorGroupMeasurement et qui ont la clef indicatorGroup
                    # sont conservées.
                    if geogLocation is None or (
                            geogLocation is not None and measurement in self.locationMeasurement[geogLocation]):
                        # idem que le test précédent
                        if unit is None or (unit is not None and measurement in self.unitMeasurement[unit]):
                            # idem que le test précédent
                            measurementsList += [measurement] # ajoute les mesures qui ont passé l'ensemble des tests
                            # à la liste des mesures retournées
    # cas où indicatorGroup n'est pas nul (idem que pour commodityGroup non nul)
        elif indicatorGroup is not None:
            for measurement in self.indicatorGroupMeasurement[indicatorGroup]:
                if commodityGroup is None or (
                        commodityGroup is not None and measurement in self.commodityGroupMeasurement[
                    commodityGroup]):
                    if geogLocation is None or (
                            geogLocation is not None and measurement in self.locationMeasurement[geogLocation]):
                        if unit is None or (unit is not None and measurement in self.unitMeasurement[unit]):
                            measurementsList += [measurement]
    # cas où geogLocation n'est pas nul (idem que pour commodityGroup non nul)
        elif geogLocation is not None:
            for measurement in self.locationMeasurement[geogLocation]:
                if commodityGroup is None or (
                        commodityGroup is not None and measurement in self.commodityGroupMeasurement[
                    commodityGroup]):
                    if indicatorGroup is None or (
                            indicatorGroup is not None and measurement in self.indicatorGroupMeasurement[
                        indicatorGroup]):
                        if unit is None or (unit is not None and measurement in self.unitMeasurement[unit]):
                            measurementsList += [measurement]
    # cas où unit n'est pas nul (idem que pour commodityGroup non nul)
        elif unit is not None:
            for measurement in self.unitMeasurement[unit]:
                if commodityGroup is None or (
                        commodityGroup is not None and measurement in self.commodityGroupMeasurement[
                    commodityGroup]):
                    if indicatorGroup is None or (
                            indicatorGroup is not None and measurement in self.indicatorGroupMeasurement[
                        indicatorGroup]):
                        if geogLocation is None or (
                                geogLocation is not None and measurement in self.locationMeasurement[geogLocation]):
                            measurementsList += [measurement]
    # On a effectué le test de non nulité sur les 4 paramètres afin de ne jamais se retrouver avec une boucle for
    # vide, mais aussi afin d'être sûrs que si 3 des paramètres sont nuls, on entre bien dans une des boucles et
    # le retour de la méthode n'est pas vide peut importe les paramètres entrés alors qu'il ne doit pas l'être

        for m in measurementsList:
            print(m.describe()) # on ne retourne pas les mesures directement (ie leur adresse mémoire) mais leur
            # description

