from abc import ABC, abstractmethod


class Describable(ABC): # classe abstraite qui impose le codage d'une méthode describe dans les autres classes sur
    # projet dans le but final de retourner des valeur lisibles des mesures dans la méthode find de FoodCropsDataset

    def __init__(self):
        super().__init__()

    @abstractmethod
    def describe(self):
        pass
