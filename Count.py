from Unit import Unit


class Count(Unit): # hérite de Unit

    def __init__(self, id: int, what: str, name: str = "Count"):
        super().__init__(id, name)
        self.what = what

    def describe(self): # la méthode est adpatée à l'appel de describe appliqué à une mesure
        return self.name + " " + self.what