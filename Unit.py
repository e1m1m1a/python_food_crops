from abc import ABC
from Describable import Describable


class Unit(Describable, ABC): # hérite de Describable et est abstraite
    def __init__(self, id: int, name: str):
        super().__init__()
        self.id = id
        self.name = name

    def describe(self): # n'implémente toujours pas la méthode describe
        pass
