from CommodityGroup import CommodityGroup
from Describable import Describable


class Commodity(Describable): # hérite de Describable

    def __init__(self, group: CommodityGroup, id: int, name: str):
        super().__init__()
        self.id = id
        self.name = name
        self.group = group

    def describe(self): # la méthode est adpatée à l'appel de describe appliqué à une mesure
        return "of " + self.name + " (" + self.group.name.lower() + " commodity)"

