from Unit import Unit


class Price(Unit): # hérite de Unit

    def __init__(self, id: int, name: str = "Price"):
        super().__init__(id, name)

    def describe(self): # la méthode est adpatée à l'appel de describe appliqué à une mesure
        return self.name