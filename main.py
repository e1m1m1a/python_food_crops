from FoodCropsDataset import FoodCropsDataset
from CommodityGroup import CommodityGroup
from IndicatorGroup import IndicatorGroup
from Price import Price

fcd = FoodCropsDataset(dict(), dict(), dict(), dict())
fcd.load("FeedGrains.csv")

# test 0
print(fcd.findMeasurements(None, None, None, None))
# Vérifie que le cas particulier de l'ensemble des arguments nuls fonctionne et renvoie bien l'ensemble des mesures
# Le nombre de mesures étant trop élevé, la fenêtre d'affichage affiche peut de valeurs.
# Cependant, on voit que les mesures affichées se suivent et qu'avec plus de place (ou en sauvegardant les mesures dans
# un fichiers csv), on récupèrerait l'ensemble des mesures présentes dans le fichier initial. Certaines mesures
# n'apparaissent pas (par exemple, on passe de 463116 à 463113), mais ceci est dû à la suppression des mesures contenant
# des NaN

print("____________________") # séparateur pour les différents tests

# test 1
commodityGroup1 = CommodityGroup.OATS
indicatorGroup1 = IndicatorGroup.PRICES
unit1 = Price(4)
geogLocation1 = "United States"
print(fcd.findMeasurements(commodityGroup1, indicatorGroup1, geogLocation1, unit1))
# Vérifie que le code tourne bien et renvoie des valeurs (choix d'arguments compatibles pour certaines mesures).

print("____________________")

# test 2
geogLocation2 = "Argentina"
print(fcd.findMeasurements(None, None, geogLocation2, None))
# Vérifie que la recherche à partir de l'argument geogLocation fonctionne.

print("____________________")

# test 3
unit3 = Price(1)
print(fcd.findMeasurements(None, None, None, unit3))
# Vérifie que la recherche à partir de l'argument unit fonctionne.

print("____________________")

# test 4
commodityGroup4 = CommodityGroup.ANIMAL_PROTEIN_FEEDS
indicatorGroup4 = IndicatorGroup.SUPPLY_AND_USE
print(fcd.findMeasurements(commodityGroup4, indicatorGroup4, None, None))
# vérifie que la recherche à partir des arguments commodity group et indicator group fonctionne.
# Renvoie None comme attendu (le commodity group "animal protein feeds et l'indicator group "supply and use" ne peuvent
# pas correspondre).

print("____________________")

# test 5
commodityGroup5 = CommodityGroup.ANIMAL_PROTEIN_FEEDS
print(fcd.findMeasurements(commodityGroup5, None, None, None))
# Permet de vérifier que l'affirmation précédente est vraie et qu'il ne s'agit pas d'un problème du code ne
# permettant pas de retrouver les mesures de commodity group "animal protein feeds".