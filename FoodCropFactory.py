from Commodity import Commodity
from CommodityGroup import CommodityGroup
from Count import Count
from Indicator import Indicator
from IndicatorGroup import IndicatorGroup
from Measurement import Measurement
from Ratio import Ratio
from Surface import Surface
from Unit import Unit
from Weight import Weight
from Volume import Volume
from Price import Price


class FoodCropFactory:

    def __init__(self, unitsRegistry: dict, indicatorsRegistry: dict, commodityRegistry: dict):
        self.unitsRegistry = unitsRegistry
        self.indicatorsRegistry = indicatorsRegistry
        self.commodityRegistry = commodityRegistry  # {(id : commodity)}

    def createPrice(self, idP: int):
        if idP not in self.unitsRegistry: # on vérifie que l'instance associée n'est pas déjà créée
            self.unitsRegistry[idP] = Price(idP) # dans le cas où elle n'est pas créée on ajoute idP dans les clés du
            # dictionnaire et on lui associe comme valeur une instance de Price associé à l'idP
        return self.unitsRegistry[idP]

    def createVolume(self, idV: int): # idem que createPrice
        if idV not in self.unitsRegistry:
            self.unitsRegistry[idV] = Volume(idV)#
        return self.unitsRegistry[idV]

    def createWeight(self, idW: int, weight: float): # idem que createPrice
        if idW not in self.unitsRegistry:
            self.unitsRegistry[idW] = Weight(idW, weight)
        return self.unitsRegistry[idW]

    def createSurface(self, idS: int): # idem que createPrice
        if idS not in self.unitsRegistry:
            self.unitsRegistry[idS] = Surface(idS)
        return self.unitsRegistry[idS]

    def createCount(self, idC: int, what: str): # idem que createPrice
        if idC not in self.unitsRegistry:
            self.unitsRegistry[idC] = Count(idC, what)
        return self.unitsRegistry[idC]

    def createRatio(self, idR: int): # idem que createPrice
        if idR not in self.unitsRegistry:
            self.unitsRegistry[idR] = Ratio(idR)
        return self.unitsRegistry[idR]

    def createCommodity(self, group: CommodityGroup, idCom: int, name: str): # idem que createPrice
        if idCom not in self.commodityRegistry:
            self.commodityRegistry[idCom] = Commodity(group, idCom, name)
        return self.commodityRegistry[idCom]

    def createIndicator(self, idInd: int, frequency: int, freqDesc: str, geogLocation: str,
                        indicatorGroup: IndicatorGroup, unit: Unit): # idem que createPrice
        if idInd not in self.indicatorsRegistry:
            self.indicatorsRegistry[idInd] = Indicator(idInd, frequency, freqDesc, geogLocation,
                                                       indicatorGroup, unit)
        return self.indicatorsRegistry[idInd]

    def createMeasurement(self, idM: int, year: int, value: float, timeperiodId: int, timeperiodDesc: str,
                          commodity: Commodity, indicator: Indicator): # Cette fois-ci, il n'y a pas de test car il
        # n'y a pas de dictionnaire vérifiant l'unicité des mesures (car chaque mesure est unique). On doit donc
        # créer l'instance automatiquement.
        return Measurement(idM, year, value, timeperiodId, timeperiodDesc, commodity, indicator)

