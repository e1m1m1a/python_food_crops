# python_food_crops
Manuella Thomet et Emma Pruvost

## Environnement de développement

* **IDE** : PyCharm

Nous travaillions sur spider par le passé, mais avons choisi PyCharm pour sa simplicité d'usage avec Git. De plus, une de nous avait déjà travaillé avec un IDE de jetBrains et GitHub pour la réalisation d'un projet java. Ainsi, l'interface était plus facile à appréhender.

* **Python version** : 3.9 (avec conda)

Nous ne pouvions pas travailler en POO avec python 2.7 car nous obtenions des erreurs. Par exemple, nous ne pouvions pas faire appel aux méthodes *super* pour réaliser les héritages. Nous avons donc installé la dernière version de python disponible, puis nous avons modifié l'interpreter de notre projet dans PyCharm.

* **Version control** : GitLab

## Mode d'emploi

Afin d'exécuter le programme, il suffit d'exécuter le code présent dans la **classe _main_** du projet. Nous avons rédigé dans cette classe **6 jeux de tests**.

Si l'on ne souhaite pas exécuter *main*, on peut utiliser le code ci-dessous. (Attention :  ces commandes sont une première vérification du fonctionnement mais ne considèrent que le cas où l'ensemble des paramètres de la méthode *find* sont nuls.)

```
from FoodCropsDataset import FoodCropsDataset
fcd = FoodCropsDataset(dict(), dict(), dict(), dict())
fcd.load("FeedGrains.csv")
print(fcd.findMeasurements(None, None, None, None))
```

## Répartition des tâches

Lors des premières séances nous avons travaillé chacune de notre côté afin de créer les classes les plus simples (les classes héritant de *Enum*, *Unit* et ses classes dérivées) que nous nous sommes répartie. Nous les avons ajouté dans un projet sur un poste commun. 

Durant la suite des séances nous avons travaillé en commun sur ce poste et avons discuté des méthodes qui nous posaient problèmes. Cette phase nous a permis de réellement comprendre et cerner le projet.  

Nous avons ensuite utilisé GitLab pendant les vacances pour pouvoir travailler toutes les deux en parallèle sur le projet. C’est lors de ces vacances et lors de la reprise des cours que nous avons pu réaliser les méthodes plus complexes présente dans FoodCropsDataset et FoodCropFactory. Nous avons chacune travaillé sur une version de la méthode find de FoodCropsDataset, car nous avions différentes idées et souhaitions tester la meilleure. Cependant suite à une mauvaise manipulation lors d’un update nous avons perdu une des versions. En dehors des séances de cours, nous nous sommes retrouvées deux fois pour finaliser le projet.

Graphique des commit effectués :

![evolution_projet.png](https://gitlab.com/e1m1m1a/python_food_crops/-/raw/master/evolution_projet.png)

## Problèmes rencontrés

#### Problèmes techniques
* Difficultés d'exécution à cause d'une **version de python trop ancienne** et nécessité de la mettre à jour.
* Difficulté de mise en oeuvre de la gestion de versions avec GitLab et plus particulièrement des **droits de modification**.
#### Problèmes de compréhension
* La compréhension du **diagramme des classes** et en particulier de l'utilisation des **dictionnaires** dans le projet car nous n'avions jamais travaillé avec des dictionnaires. Cela nous a demandé quelques efforts de documentation.
* La compréhension de la classe **Enum**, jamais utilisée encore, impliquant des erreurs liées à une mauvaise utilisation du type *Enum* et de son lien avec les entiers et chaînes de caractères qu'il définit.
#### Problèmes de codage
Erreurs diverses de codage corrigées avec la **signalisation** des problèmes et avec des **exécutions** progressives.
* Dans *FoodCropFactory*, nous avons supprimé les **dictionnaires vides mis par défaut** initialement, afin qu'entre deux exécutions les nouvelles valeurs non vides des dictionnaires ne deviennent pas celles par défaut pour l'exécution suivante.
* Dans *FoodCropsDataset*, nous avons eu des difficultés à trouver une solution pour **créer le bon type d'unité**. Nous avons pensé à créer en avance l'ensemble des unités existantes. Finalement, nous avons décidé de rester davantage fidèles aux attentes des fonctionnalités, et de créer les unités seulement au moment de leur première apparition dans le chargement du fichier *csv* en étudiant toutes les chaînes de caractères possibles. Nos tests permettent donc bien de traiter le fichier. Cependant, si le fichier venait à changer et que de nouvelles unités apparaissaient (par exemple un prix sous forme de "Won"), le code ne permettrait pas de créer les bonnes unités.
* Compréhension de la **relation module / classe** qui existe et remplacement de *import class* par *from module import class*. 
* Nous avons rencontré des erreurs du type **_iterable errors_** et **_key errors_**. Nous avons en particulier découvert l'existence des **valeurs _NaN_** qui, présentes dans le fichier *csv* proposé, nous provoquait des erreurs. Pour y remédier, nous avons trouvé la méthode *dropna* permettant de supprimer les lignes du fichier en contenant. Nous avions également trouvé une méthode permettant de conserver les lignes en remplaçant les *NaN* par 0, mais étant donné la taille du fichier *csv*, nous n'avons pas considéré nécessaire de les conserver.
* Nous avons rencontré des erreurs **_None Type_** dans les méthodes *describe* pour la concaténation et la conversion de types. Dans le cas de *Indicator*, la concaténation de *None Types* était parfois due à des données vides du fichier pour lesquelles nous avons ajouté des tests prévoyant ces situations.
* Alors que notre code fonctionnait pour certains tests appliqué à la méthode *find*, nous avons rélalisé qu'il ne fonctionnait pas pour la mise en paramètre de *unit* et *geogLocation*. Pour corriger cela, nous avons ajouté des tests en amont de la recherche sur ces deux paramètres dans *find*. Ils permettent d'écarter les **erreurs dues à un emplacement de stockage différent** pour des objets en réalité identiques, ou encore **celles dues à la présence d'un espace invisible** dans le *csv* mais qui n'indique pas une localisation différente.

## État actuel du projet

Le projet fonctionne correctement outre la **suppression de certaines mesures** dues au NaN, pour lequel nous n'avons pas trouvé d'autre solution. Cependant, il est relativement lent avec un **ordre de grandeur de la minute pour l'exécution** du *load* et d'une recherche. 

Nous avons voulu essayer de condenser le code de la méthode *find* pour les cas où au moins un des paramètres n'est pas nul. Le résultat obtenu était bien pire (8 minutes d'exécution pour le test1 du *main*). La portion de code qui avait été modifiée est celle-ci dessous.

```
for key, value in self.indicatorGroupMeasurement.items():
        for measurement in value:
                if commodityGroup is None or (commodityGroup is not None and measurement in self.commodityGroupMeasurement[commodityGroup]):
                        if indicatorGroup is None or (indicatorGroup is not None and measurement in self.indicatorGroupMeasurement[indicatorGroup]):
                                if geogLocation is None or (geogLocation is not None and measurement in self.locationMeasurement[geogLocation]):
                                        if unit is None or (unit is not None and measurement in self.unitMeasurement[unit]):
                                                measurementsList += [measurement]
```
