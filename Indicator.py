from Unit import Unit
from IndicatorGroup import IndicatorGroup
from Describable import Describable


class Indicator(Describable): # hérite de Describable

    def __init__(self, id: int, frequency: int, frequencyDesc: str, geogLocation: str, indicatorGroup: IndicatorGroup, unit: Unit):
        super().__init__()
        self.id = id
        self.frequency = frequency
        self.frequencyDesc = frequencyDesc
        self.geogLocation = geogLocation
        self.indicatorGroup = indicatorGroup
        self.unit = unit

    def describe(self): # la méthode est adpatée à l'appel de describe appliqué à une mesure
        f = "?" # permet d'afficher "?" à la place de frequency si frequency est inconnu
        if self.frequency is not None:
            f = str(self.frequency) # conversion de type réalisable uniquement si frequency n'est pas None (d'où le if)
        fd = "?" # idem que pour frequency
        if self.frequencyDesc is not None:
            fd = str(self.frequencyDesc)
        gl = "?" # idem que pour frequency
        if self.geogLocation is not None:
            gl = str(self.geogLocation)
        return self.unit.describe() + " (" "belongs to " + self.indicatorGroup.name.lower() + " indicator type, measured " + f + " time(s) " + fd + " in " + gl + ")"