from Commodity import Commodity
from Indicator import Indicator
from Describable import Describable


class Measurement(Describable): # hérite de Describable

    def __init__(self, idM: int, year: int, value: float, timeperiodId: int, timeperiodDesc: str,
                 commodity: Commodity, indicator: Indicator):
        super().__init__()
        self.id = idM
        self.year = year
        self.value = value
        self.timeperiodId = timeperiodId
        self.timeperiodDesc = timeperiodDesc
        self.commodity = commodity
        self.indicator = indicator

    def describe(self): # correspond au describe qui va être directement retourné dans la console
        # On ajoute le numéro de la mesure (qui ne sert pas vraiement à la décrire) afin de vérifier que
        # le programme fonctionne mais aussi pour distinguer plus facilement les mesures renvoyées.
        return "Measure n°" + str(self.id) + " : " + str(self.value) + " " + self.indicator.describe() + " " + self.commodity.describe() + " from " + self.timeperiodDesc + " " + str(self.year)
